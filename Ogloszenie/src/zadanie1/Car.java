package zadanie1;

public class Car {
	
	protected String brand;
	protected double mileage;
	protected int vintage;
	protected boolean damaged;
	protected boolean usednew;
	
	Car() {}
	
	public Car(String brand, double mileage, int vintage,
			   boolean damaged, boolean usednew)	
	{
		this.brand = brand;
		this.mileage = mileage;
		this.vintage = vintage;
		this.damaged = damaged;
		this.usednew = usednew;
	}

	public boolean isDamaged() {
		if(damaged == true) { System.out.println("Uszkodzony"); }
		else if(damaged == false) { System.out.println("Nie uszkodzony"); }
		return damaged;
	}

	public void setDamaged(boolean damaged) {
		this.damaged = damaged;
	}

	public boolean isUsedNew() {
		if(usednew == true) { System.out.println("Uzywany"); }
		else if(usednew) { System.out.println("Nowy"); }
		return usednew;
	}

	public void setUsedNew(boolean usednew) {
		this.usednew = usednew;		
	}

	@Override
	public String toString() {
		return "Car [brand=" + brand + ", mileage=" + mileage + ", vintage=" + vintage + ", damaged=" + damaged
				+ ", usednew=" + usednew + "]";
	}

	
}