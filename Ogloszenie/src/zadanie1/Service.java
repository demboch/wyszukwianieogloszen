package zadanie1;

import java.util.LinkedList;
import java.util.Scanner;

public class Service {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		
		boolean end = false;
		int choice;
		
		Ad adList = new Ad();
		Ad car1 = new Ad("1 Audi",45678.91,2014,true,false,250000.00,"Pomorskie","Gdansk");
		Ad car2 = new Ad("2 Bmw",93466.87,2013,true,false,200000.00,"Pomorskie","Gdansk");
		Ad car3 = new Ad("3 Mercedes",164788.34,2012,true,false,150000.00,"Pomorskie","Gdansk");
		Ad car4 = new Ad("4 Porsche",28765.23,2015,true,false,450000.00,"Pomorskie","Gdansk");
		Ad car5 = new Ad("5 Opel",210789.74,2013,true,false,70000.00,"Pomorskie","Gdansk");
		
		LinkedList<Ad> list = new LinkedList<Ad>();
		
		System.out.println("Wybierz samochod: \n");

		list.add(car1);
		list.add(car2);
		list.add(car3);
		list.add(car4);
		list.add(car5);
		do
		{
			for(int i=0; i<list.size(); i++)
			{
				adList = list.get(i);
				adList.Brand(adList);
			}
			System.out.println("6 Zakoncz ");
			
			choice = in.nextInt();
			adList = list.get(choice-1);
			adList.About();
			
			System.out.println("\nWybierz inne ogloszenie: \n");
			
			if (choice == 6)
			{
				end = true;
			}
		}
		while(!end);
	}
}

