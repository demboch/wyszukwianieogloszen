package zadanie1;

import java.util.ArrayList;
import java.util.Date;

public class Ad extends Car {
	
	private double price;
	private String region;
	private String city;
	private Date date;
	
	Ad(){}
	
	public Ad(String brand, double mileage, int vintage,boolean damaged, 
			  boolean usednew, double price, String region, String city)
	{
		super(brand, mileage, vintage, damaged, usednew);
		this.price = price;
		this.region = region;
		this.city = city;
		this.date = new Date();
	}

	public void Brand(Ad brand)
	{
		System.out.println(brand.brand);
	}
	
	public void About()
	{
		Ad get = new Ad();
		System.out.println("Model: " + brand);
		System.out.println("Cena: " + price);
		System.out.println("Rocznik: " + vintage);
		System.out.println("Region: " + region);
		System.out.println("Miasto: " + city);
		System.out.println("Przebieg: " + mileage);
		System.out.println("Uszkodzenia: " + get.isDamaged());
		System.out.println("Stan: " + get.isUsedNew());
		System.out.println("Data: " + date);
	}
}